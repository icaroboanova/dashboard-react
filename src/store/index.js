import { createStore } from "redux";

const ESTADO_INICIAL = {
  tabela: {
    titulo: "",
    valores: [],
    colunas: {
      produtos: [
        { title: "Id", field: "id", editable: "never" },
        { title: "Nome", field: "nome" },
        { title: "Valor", field: "valor", type: "numeric" }
      ],
      clientes: [
        { title: "Id", field: "id", editable: "never" },
        { title: "Nome", field: "nome" }
      ],
      vendas: [
        { title: "Id", field: "id", editable: "never" },
        { title: "Nome Cliente", field: "nome_cliente", editable: "never" },
        { title: "Nome Produto", field: "nome_produto", editable: "never" },
        {
          title: "Quantidade",
          field: "quantidade",
          editable: "onUpdate",
          type: "numeric"
        },
        { title: "Data", field: "data", editable: "never" },
        {
          title: "Valor Total",
          field: "valor",
          editable: "never",
          type: "numeric"
        }
      ]
    }
  }
};

function reducer(state, action) {
  let retorno = ESTADO_INICIAL;

  if (action.componente === "tabela") {
    if (action.data !== undefined) {
      retorno.tabela.valores = JSON.parse(action.data);
    }

    if (action.title !== undefined) {
      retorno.tabela.titulo = action.title;
    }
  }

  return retorno;
}

const store = createStore(reducer);

export default store;
