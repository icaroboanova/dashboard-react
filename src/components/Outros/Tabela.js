import React from "react";
import MaterialTable from "material-table";
import { connect } from "react-redux";

export default connect(state => ({ parametros: state.tabela }))(
  function Tabela({ parametros }) {
    return (
      <MaterialTable
        title={parametros.titulo}
        columns={parametros.colunas[parametros.titulo.toString().toLowerCase()]}
        data={parametros.valores}
        editable={{
          onRowAdd: newData =>
            new Promise(resolve => {
              if (parametros.titulo === "Clientes") {
                if (newData.nome !== undefined) {
                  fetch("http://localhost:8080/dashboard-react-api/cliente", {
                    method: "POST",
                    body: JSON.stringify({
                      nome: newData.nome
                    })
                  }).then(resposta => {
                    window.location.reload();
                  });
                } else {
                  window.location.reload();
                }
              }

              if (parametros.titulo === "Produtos") {
                if (newData.nome !== undefined) {
                  fetch("http://localhost:8080/dashboard-react-api/produto", {
                    method: "POST",
                    body: JSON.stringify({
                      nome: newData.nome,
                      valor: newData.valor !== undefined ? newData.valor : 0
                    })
                  }).then(resposta => {
                    window.location.reload();
                  });
                } else {
                  window.location.reload();
                }
              }

              if (parametros.titulo === "Vendas") {
                alert("Operação não permitida");
                window.location.reload();
              }
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise(resolve => {
              if (parametros.titulo === "Clientes") {
                if (newData.nome !== undefined) {
                  fetch(
                    "http://localhost:8080/dashboard-react-api/cliente/update/" +
                      oldData.id.toString(),
                    {
                      method: "POST",
                      body: JSON.stringify({
                        id: oldData.id,
                        nome: newData.nome
                      })
                    }
                  ).then(resposta => {
                    window.location.reload();
                  });
                } else {
                  window.location.reload();
                }
              }
              if (parametros.titulo === "Produtos") {
                if (newData.nome !== undefined) {
                  fetch(
                    "http://localhost:8080/dashboard-react-api/produto/update/" +
                      oldData.id.toString(),
                    {
                      method: "POST",
                      body: JSON.stringify({
                        id: oldData.id,
                        nome: newData.nome,
                        valor:
                          newData.valor !== "" && newData.valor !== undefined
                            ? newData.valor
                            : 0
                      })
                    }
                  ).then(resposta => {
                    window.location.reload();
                  });
                } else {
                  window.location.reload();
                }
              }

              if (parametros.titulo === "Vendas") {
                if (newData.quantidade !== undefined) {
                  fetch(
                    "http://localhost:8080/dashboard-react-api/venda/update/" +
                      oldData.id.toString(),
                    {
                      method: "POST",
                      body: JSON.stringify({
                        quantidade:
                          newData.quantidade !== "" &&
                          newData.quantidade !== undefined
                            ? newData.quantidade
                            : 0
                      })
                    }
                  ).then(resposta => {
                    window.location.reload();
                  });
                } else {
                  window.location.reload();
                }
              }
            }),
          onRowDelete: oldData =>
            new Promise(resolve => {
              if (parametros.titulo === "Clientes") {
                fetch(
                  "http://localhost:8080/dashboard-react-api/cliente/delete/" +
                    oldData.id.toString(),
                  {
                    method: "POST",
                    body: JSON.stringify({})
                  }
                ).then(resposta => {
                  window.location.reload();
                });
              }
              if (parametros.titulo === "Produtos") {
                fetch(
                  "http://localhost:8080/dashboard-react-api/produto/delete/" +
                    oldData.id.toString(),
                  {
                    method: "POST",
                    body: JSON.stringify({})
                  }
                ).then(resposta => {
                  window.location.reload();
                });
              }

              if (parametros.titulo === "Vendas") {
                fetch(
                  "http://localhost:8080/dashboard-react-api/venda/delete/" +
                    oldData.id.toString(),
                  {
                    method: "POST",
                    body: JSON.stringify({})
                  }
                ).then(resposta => {
                  window.location.reload();
                });
              }
            })
        }}
      />
    );
  }
);
