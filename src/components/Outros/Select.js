import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


export default function SimpleSelect(parametros) {
    const useStyles = makeStyles(theme => ({
        formControl: {
          width: parametros.largura,
          margin: theme.spacing(1),
          minWidth: 120,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
      }));

  const classes = useStyles();
  const [valor, setValor] = React.useState('');

  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  const handleChange = event => {
    setValor(event.target.value);
  };

  return (
    <div>
      <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel ref={inputLabel} id="demo-simple-select-outlined-label">
          {parametros.label}
        </InputLabel>
        <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          value={valor}
          onChange={handleChange}
          labelWidth={labelWidth}
        >
        <MenuItem value={0}>Nenhum</MenuItem>
          {parametros.opcoes.map((opcao, key) => {
               return <MenuItem key={key} value={opcao.valor}>{opcao.label}</MenuItem>
          })}
        </Select>
      </FormControl>
    </div>
  );
}