import Produtos from "./views/Produtos";
import Clientes from "./views/Clientes";
import Venda from "./views/Venda";
import Vendas from "./views/Vendas";
import Refresh from "views/Refresh";

var routes = [
  {
    path: "/sale",
    name: "Nova Venda",
    icon: "ni ni-tv-2 text-primary",
    component: Venda
  },
  {
    path: "/sales",
    name: "Vendas",
    icon: "ni ni-tv-2 text-primary",
    component: Vendas
  },
  {
    path: "/products",
    name: "Produtos",
    icon: "ni ni-tv-2 text-primary",
    component: Produtos
  },
  {
    path: "/clients",
    name: "Clientes",
    icon: "ni ni-tv-2 text-primary",
    component: Clientes
  },
  {
    path: "/refresh",
    component: Refresh
  }
];
export default routes;
