import React from "react";
import { Card as CardReact, Container } from "reactstrap";
import Header from "../components/Headers/Header.js";
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";

export default class Venda extends React.Component {
  constructor() {
    super();

    this.state = {
      cliente: {
        nome: "",
        id: ""
      },
      produtos: [],
      produtosSelecionados: [],
      clientes: [],
      clienteSelecionado: "",
      produtosVendidos: []
    };

    this.addProduto = this.addProduto.bind(this);
    this.confirmarForm = this.confirmarForm.bind(this);
  }

  componentDidMount() {
    let clientes = this.carregarClientes();
    let produtos = this.carregarProdutos();

    clientes = JSON.parse(clientes);
    produtos = JSON.parse(produtos);

    this.setState({ clientes, produtos });
  }

  carregarProdutos() {
    return this.httpGet("http://localhost:8080/dashboard-react-api/produtos");
  }

  carregarClientes() {
    return this.httpGet("http://localhost:8080/dashboard-react-api/clientes");
  }

  httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
  }

  addProduto() {
    if (
      this.state.produtosSelecionados.indexOf(
        document.getElementById("combo-box-produtos").value
      ) === -1
    ) {
      let produtosSelecionados = this.state.produtosSelecionados;
      produtosSelecionados.push(
        document.getElementById("combo-box-produtos").value
      );
      this.setState({ produtosSelecionados: produtosSelecionados });
      document.getElementById("combo-box-produtos").value = "";
    }
  }

  async confirmarForm() {
    if (this.state.produtosSelecionados.length > 0) {
      let produtos = [];

      for (let cc = 0; cc < this.state.produtosSelecionados.length; cc++) {
        let obj = {};
        for (let tt = 0; tt < this.state.produtos.length; tt++) {
          if (
            this.state.produtosSelecionados[cc] === this.state.produtos[tt].nome
          ) {
            obj = this.state.produtos[tt];
            break;
          }
        }
        obj.quantidade = document.getElementById("quantidade_" + cc).value;
        produtos.push(obj);
      }

      let cliente = 0;
      for (let xx = 0; xx < this.state.clientes.length; xx++) {
        if (
          this.state.clientes[xx].nome ===
          document.getElementById("combo-box-clientes").value
        ) {
          cliente = this.state.clientes[xx].id;
          break;
        }
      }

      await this.setState({
        clienteSelecionado: cliente,
        produtosVendidos: produtos
      });

      fetch("http://localhost:8080/dashboard-react-api/venda", {
        method: "POST",
        body: JSON.stringify({
          cliente: this.state.clienteSelecionado,
          produtosVendidos: JSON.stringify(this.state.produtosVendidos)
        })
      }).then(resposta => {
        this.props.history.push("/sales");
      });
    }
  }

  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          <CardReact className="shadow">
            <Card>
              <CardContent>
                <CardHeader
                  style={{ justifyContent: "center", alignItems: "center" }}
                >
                  Nova Venda
                </CardHeader>
                <Autocomplete
                  id="combo-box-clientes"
                  options={this.state.clientes}
                  //value={this.state.teste}
                  getOptionLabel={option => option.nome}
                  style={{ width: "100%", marginBottom: "2%" }}
                  name="clientes"
                  renderInput={params => (
                    <TextField
                      id="cliente"
                      {...params}
                      label="Selecionar Cliente"
                      variant="outlined"
                    />
                  )}
                />

                <Autocomplete
                  id="combo-box-produtos"
                  options={this.state.produtos}
                  getOptionLabel={option => option.nome}
                  style={{
                    width: "92%",
                    float: "left",
                    marginBottom: "1%"
                  }}
                  name="produto"
                  renderInput={params => (
                    <TextField
                      {...params}
                      label="Selecionar Produto"
                      variant="outlined"
                    />
                  )}
                />
                <IconButton
                  onClick={this.addProduto}
                  style={{ float: "right" }}
                  aria-label="add"
                >
                  <AddIcon />
                </IconButton>
                <List component="nav" aria-label="mailbox folders">
                  {this.state.produtosSelecionados.map((produto, key) => {
                    return (
                      <div key={key}>
                        <ListItem style={{ width: "71%", float: "left" }}>
                          <ListItemText primary={produto} />
                        </ListItem>
                        <TextField
                          style={{ width: "21%", float: "left" }}
                          id={
                            "quantidade_" +
                            this.state.produtosSelecionados.indexOf(produto)
                          }
                          label="Quantidade"
                          type="number"
                          InputLabelProps={{
                            shrink: true
                          }}
                        />
                        <IconButton
                          style={{ float: "right" }}
                          aria-label="delete"
                        >
                          <RemoveIcon />
                        </IconButton>
                      </div>
                    );
                  })}
                </List>

                <Button
                  onClick={this.confirmarForm}
                  style={{
                    float: "right",
                    marginTop: "4%",
                    marginBottom: "2%"
                  }}
                  variant="contained"
                  color="secondary"
                >
                  Vender!
                </Button>
              </CardContent>
            </Card>
          </CardReact>
        </Container>
      </>
    );
  }
}
