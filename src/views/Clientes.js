import React from "react";
import { Card, Container } from "reactstrap";
import Header from "../components/Headers/Header.js";
import Tabela from "../components/Outros/Tabela";
import store from "../store/index";

export default class Clientes extends React.Component {
  constructor() {
    super();

    this.state = {
      teste: "a"
    };
  }

  componentDidMount() {
    let data = this.httpGet(
      "http://localhost:8080/dashboard-react-api/clientes"
    );

    store.dispatch(this.carregarDados(data));

    this.setState({
      teste: "b"
    });
  }

  carregarDados(dados) {
    return {
      type: "carregar_dados",
      data: dados,
      title: "Clientes",
      componente: "tabela"
    };
  }

  httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
  }

  render() {
    return (
      <>
        <Header />
        <Container className="mt--7" fluid>
          <Card className="shadow">
            <Tabela teste={this.state.teste}></Tabela>
          </Card>
        </Container>
      </>
    );
  }
}
